# lambda-javascript

This lambda is an exmaple for a dockerized environments, is prepared to use AWS Parameter Store and is ready to deploy to our AWS account automatically.

### Preconditions

- Complete your env-example.
- Use a Makefile to do mThis lambda use a Makefile to do more easy the initial setup.
- To deploy automatically with the pipeline, you'll need config your AWS keys in Gitlab env variables, more info, [here](https://docs.gitlab.com/ee/ci/variables/)

### Dependencies

Yo need:

- Docker

To init this project you can run:

```shell
make start
```

### Linting

To comply Airbnb standards:

```shell
make lint-fix
```

### Testing

To run tests:

``` sh
make test
```

### To debug your local container

Please use:

```shell
make exec
```

### To clean your local environment

Please use:

```shell
make clean
```

### To delete your stop environment

Please use:

```shell
make clean-f
```

### To delete all your dockerized environment (incluiding other docker images, containers, etc)

Please use:

```shell
make clean-all
```

### To delete your Lambda from AWS (just admins)

Please use:

```shell
1. make aws-config
```

```shell
2. make just-admin-remove-lambda
```

### To invoke locally

1. Set an `event.json` file into the root directory with your `json event` to be used by the lambda
2. Type:

```shell
make invoke
```

### Credits

`jlcdche@gmail.com`
