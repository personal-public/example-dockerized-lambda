DOCKER_COMPOSE=docker-compose -f ./infrastructure/docker-compose.yml
SERVICE_NAME=lambda-javascript
FUNCTION_NAME=functionName
#set ENVIRONMENT_FILE=.env after use make start
ENVIRONMENT_FILE=

ifneq (“$(wildcard ${ENVIRONMENT_FILE})“,”“)
    include ${ENVIRONMENT_FILE}
endif

set-env:
	@test -e .env || cp .env-example .env

install:
	docker run -it --rm -w /app -v $(PWD):/app node:12.18.0 yarn install

docker-up:
	$(DOCKER_COMPOSE) up

qc:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) yarn test
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) yarn lint

test:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) yarn test

start: set-env install docker-up

lint-fix:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) yarn run lint:fix

exec:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) bash -c

clean:
	$(DOCKER_COMPOSE) down
	@sudo rm -R node_modules/

clean-f:
	$(DOCKER_COMPOSE) down -v --rmi all
	@sudo rm -R node_modules/

clean-all:
	cd devops; \
	sh docker-clean.sh;

aws-config:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) serverless config credentials --provider aws --key ${AWS_ACCESS_KEY_ID_A} --secret ${AWS_SECRET_ACCESS_KEY_A}

invoke:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) serverless invoke local --function $(FUNCTION_NAME) --path event.json -v

just-admin-remove-lambda:
	$(DOCKER_COMPOSE) exec $(SERVICE_NAME) serverless remove --stage ${ENV} --region us-west-2
