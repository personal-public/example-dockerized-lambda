#!/usr/bin/sh

AWS_PERSONAL_ACCOUNT="1234..."
AWS_EXISTS="command -v aws"
AWS_CHECK_CREDENTIALS="aws sts get-caller-identity"

if [ -f ./.env ]
then
	export $(cat ./.env | sed 's/#.*//g' | xargs)
fi

download_aws_cli()
{
	AWS_CLI_EXISTS=$($AWS_EXISTS)

	if [ -z ${AWS_CLI_EXISTS} ]; then
		curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
		sudo installer -pkg AWSCLIV2.pkg -target /
		aws --version
		rm AWSCLIV2.pkg
	else
		echo "aws has been installed"
	fi
}

set_aws_credentials()
{
	aws configure set region ${AWS_REGION}
	aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID_A}
	aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY_A}
}

check_aws_previous_installed()
{	
	AWS_CREDENTIALS=$($AWS_CHECK_CREDENTIALS)

	if echo "$AWS_CREDENTIALS" | grep "${AWS_PERSONAL_ACCOUNT}"; then
		echo "aws credentials setted"
	else
		echo "setting aws credentials"
		set_aws_credentials
	fi
}

do_aws_login()
{
	aws ecr get-login-password --region ${AWS_REGION} | docker login --username AWS --password-stdin ${CONTAINER_REGISTRY}
}

download_aws_cli
check_aws_previous_installed
do_aws_login
