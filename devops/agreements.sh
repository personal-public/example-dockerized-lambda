#!/bin/bash

MAX_COMMIT_QUANTITY=1
MAX_INSERTIONS_PER_MERGE_REQUEST=350

echo "RULE# 2: Checking branch name format..."

if [[ ! "${CI_COMMIT_REF_NAME}" = "feature/"*
        && ! "${CI_COMMIT_REF_NAME}" = "bugfix/"*
        && ! "${CI_COMMIT_REF_NAME}" = "refactor/"*
        && ! "${CI_COMMIT_REF_NAME}" = "improvement/"*
        && ! "${CI_COMMIT_REF_NAME}" = "experiment/"*
        && ! "${CI_COMMIT_REF_NAME}" = "documentation/"*
        && ! "${CI_COMMIT_REF_NAME}" = "main"* ]]; then
    echo "RULE# 2: [ERROR] Invalid branch name format: $CI_COMMIT_REF_NAME!"
    exit 1
else
    echo "RULE # 2: Checked successfully!"
fi

git pull origin main

echo "RULE # 1: Checking commits quantity..."

COMMITS_COUNT=$(git log --oneline origin/${CI_COMMIT_REF_NAME} ^origin/main | wc -l)

if [[ $COMMITS_COUNT -gt $MAX_COMMIT_QUANTITY ]];
then
    echo "RULE # 1: [ERROR] There must be only one commit, you have $COMMITS_COUNT!"
    exit 1
else
    echo "RULE # 1: Checked successfully!"
fi

INSERTIONS=$(git diff --shortstat origin/main | sed -E "s/.* ([0-9]+) insertion.*/\1/")

INSERTIONS_IGNORE=0
FILES_IGNORE=(".gitlab-ci.yml"
            "src/main.js"
            "yarn.lock")

for file in ${FILES_IGNORE[@]}
do
   let "INSERTIONS_IGNORE+=$(git diff --shortstat origin/main $file | sed -E "s/.* ([0-9]+) insertion.*/\1/")"
done
INSERTIONS=$((INSERTIONS - INSERTIONS_IGNORE))

echo "RULE # 2: Checking number of insertions..."

if [[ $INSERTIONS -gt $MAX_INSERTIONS_PER_MERGE_REQUEST ]];
then
    echo "RULE # 2: [ERROR] You exceeded the insertions limit with $INSERTIONS lines!"
    exit 1
else
    echo "RULE # 2: Checked successfully!"
fi
