const aws = require('config/aws');
const constants = require('config/constants');
const process = require('config/process');

module.exports = {
  aws,
  constants,
  process,
};
