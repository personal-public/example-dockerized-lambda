const Environment = Object.freeze({
  Develop: 'develop',
  Staging: 'staging',
  Production: 'production',
});

module.exports = Environment;
