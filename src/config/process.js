const path = require('path');

const dotenv = require('dotenv');

const Environment = require('config/constants');
const { name, version } = require('../../package.json');

const { env } = process;

const getCurrentEnv = () => (
  Object.values(Environment).includes(env.NODE_ENV) ? env.NODE_ENV : Environment.Develop
);

module.exports = {
  appName: name,
  appVersion: version,
  businessBranch: env.BUSINESS_BRANCH,
  branch: env.BRANCH,
  project: env.PROJECT,
  node: {
    env: getCurrentEnv(),
    parsedEnv: dotenv.config({ path: path.join(__dirname, '../../.env') }).parsed || { },
  },
};
