const AWS = require('aws-sdk');

const { process: { node: { parsedEnv } } } = require('config');

const {
  process: {
    node: { env }, businessBranch, branch, project, appName,
  },
  aws: {
    region,
  },
  constants: Environment,
} = require('config');

const isDevelop = () => env === Environment.Develop;

const ssm = new AWS.SSM({ region });

const ssmPath = `/${env}/${businessBranch}/${branch}/${project}/${appName}/`;

const parametersToJson = (parameters) => {
  let params = parameters.Value || parameters;
  while (typeof params === 'string') {
    params = JSON.parse(params);
  }
  return params;
};

const getParameters = async (extraPath = '', isAJsonParameter = false) => {
  if (isDevelop()) {
    const variable = extraPath;
    const value = parsedEnv[variable];
    return isAJsonParameter ? parametersToJson(value) : value;
  }

  const options = {
    Names: [`${ssmPath}${extraPath}`],
    WithDecryption: true,
  };

  try {
    const { Parameters: [parameter] } = await ssm.getParameters(options).promise();

    return isAJsonParameter
      ? parametersToJson(parameter.Value)
      : parameter.Value;
  } catch (error) {
    error.message = error.message || 'Failed to fetch parameters';
    error.status = 500;

    throw error;
  }
};

module.exports = {
  getParameters,
};
