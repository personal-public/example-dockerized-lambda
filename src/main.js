const ssm = require('infra/ssm');

const main = async (event, context) => {
  const data = await ssm.getParameters();

  return {
    statusCode: 200,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ event, context, data }),
  };
};

module.exports = {
  main,
};
